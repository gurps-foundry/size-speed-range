# GURPS Foundry Size and Speed/Range module

This module provides:
- A range ruler displaying GURPS range penalties
- Chat commands for looking up the size modifier and speed/range penalties for given values

GURPS is a trademark of Steve Jackson Games, and its rules and art are copyrighted by Steve Jackson Games. All rights are reserved by Steve Jackson Games. This game aid is the original creation of Exxar and is released for free distribution, and not for resale, under the permissions granted in the <a href="http://www.sjgames.com/general/online_policy.html">Steve Jackson Games Online Policy</a>.

## Installation
Install via Foundry by using the manifest URL:
```
https://gitlab.com/gurps-foundry/size-speed-range/-/raw/master/module.json
```
## How to use
### Range Ruler
Just use the "Measure Distance" tool under "Basic Controls". Grid units in your scene configuration need to be set to one of the supported units, below.
### Size Modifier and Speed/Range Penalty Lookup
The following two chat commands are available:
- `!ssrtsm` - Gives the size modifier for a given length. Example: `!ssrtsm 2 inches`
- `!ssrtsr` - Gives the speed/range penalty for a given distance. Example: `!ssrtsr 11 yards`

## Units
The following units of measure are supported as scene grid units and in chat commands:
- ", '', in, inch, inches
- ', ft, foot, feet
- y, yd, yard, yards
- mi, mile, miles
- cm, centimeter, centimeters
- m, meter, meters
- km, kilometer, kilometers

Metric units are converted to yards using the formula `yards = meters / 1.09361` and the table lookup is performed with the converted value.