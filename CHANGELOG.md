# Changelog:

## Version 1.0.2 - 2020-10-25

Marked the module as compatible with Foundry 0.7.5. This is only an administrative change for the benefit of the Foundry module management system.

## Version 1.0.1 - 2020-08-28

No user-facing changes, adapted the build process for better release management.

## Version 1.0.0 - 2020-07-23

### Added
- Readme with instructions and legal disclaimer
- This document

## Version 0.2.0 - 2020-07-21

### Added
- First functional implementation of the range ruler and SSRT lookup commands