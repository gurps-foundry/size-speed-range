const path = require('path');

module.exports = (env, argv) => {
    const config = {
        entry: './src/main.js',
        output: {
            path: path.resolve(__dirname),
            filename: 'gurps-foundry-ssr.bundle.js',
        }
    };

    if (argv.mode === 'development') {
        config.devtool = 'inline-source-map';
    }

    return config;
};
